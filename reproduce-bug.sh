#!/usr/bin/env bash

BLUE='\e[0;34m'
GREEN='\e[0;32m'
RESET='\e[0;0m'

info() {
  printf "${BLUE}$@${RESET}\n"
}

# Version 0.42.0
semgrep_docker_tag=returntocorp/semgrep:823803745d06df1d895fb92d2dfd6e80636ce6ae

info "No --sarif switch will result in an error."
docker run --rm -v "${PWD}:/src" $semgrep_docker_tag -f react.yml

info "Now adding the --sarif switch will result in swalling the error."
docker run --rm -v "${PWD}:/src" $semgrep_docker_tag -f react.yml --sarif
