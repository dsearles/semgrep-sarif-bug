# Semgrep SARIF Bug

This is a [SSCCE](http://www.sscce.org/) to show how Semgrep version 0.42.0 will swallow rule errors if you
add the `--sarif` switch.

## To Reproduce

* Clone this repo
* Run `./reproduce-bug.sh` (note that docker is required)
